const express = require('express');

const auth = require('../middleware/auth');
const Comment = require('../models/Comment');

const router = express.Router();

router.post('/', auth, (req, res) => {
    const commentData = {...req.body, user: req.user._id};

    const comment = new Comment(commentData);

    comment.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});


router.get('/:id', (req, res) => {
    Comment.find({post: req.params.id}).populate('user').populate('posts')
        .then(comment => {
            if (comment) res.send(comment);
            else res.sendStatus(404);
        })
        .catch(() => res.sendStatus(500));
});


module.exports = router;
