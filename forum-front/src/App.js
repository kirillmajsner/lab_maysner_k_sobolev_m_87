import React, {Component, Fragment} from 'react';
import {Container} from "reactstrap";
import {Route, Switch, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {NotificationContainer} from "react-notifications";

import {logoutUser} from "./store/actions/usersActions";

import Toolbar from "./components/UI/Toolbar/Toolbar";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Posts from "./containers/Posts/Posts";
import CreatePost from "./containers/CreatePost/CreatePost";
import SinglePost from "./containers/SinglePost/SinglePost";


class App extends Component {
    render() {
        return (
            <Fragment>
                <NotificationContainer/>
                <header>
                    <Toolbar
                        user={this.props.user}
                        logout={this.props.logoutUser}
                    />
                </header>
                <Container style={{mxarginTop: '20px'}}>
                    <Switch>
                        <Route path="/" exact component={Posts}/>
                        <Route path="/post/new" exact component={CreatePost}/>
                        <Route path="/post/:id" exact component={SinglePost}/>
                        <Route path="/register" exact component={Register}/>
                        <Route path="/login" exact component={Login}/>

                    </Switch>
                </Container>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps,)(App));
