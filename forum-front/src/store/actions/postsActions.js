import axios from "../../axios-forum";
import {push} from 'connected-react-router'

export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const CREATE_POST_SUCCESS = 'CREATE_POST_SUCCESS';
export const FETCH_ONE_POST = 'FETCH_ONE_POST';

export const fetchOnePostSuccess = post => ({type: FETCH_ONE_POST, post});
export const fetchPostsSuccess = posts => ({type: FETCH_POSTS_SUCCESS, posts});
export const createPostsSuccess = () => ({type: CREATE_POST_SUCCESS});


export const fetchOnePost = (id) => {
    return dispatch => {
        return axios.get(`/post/${id}`).then(response => {
            dispatch(fetchOnePostSuccess(response.data));
        })
    }
};


export const fetchPosts = () => {
    return dispatch => {
        return axios.get('/post').then(
            response => dispatch(fetchPostsSuccess(response.data))
        );
    };
};

export const createPost = (postData) => {
    return (dispatch, getState) => {
        const user = getState().users.user;
        if (user === null) {
            dispatch(push('/login'))
        }
        return axios.post('/post', postData, {headers: {'Authorization': user.token}}).then(
            response => dispatch(createPostsSuccess(response.data))
        )
    }
};