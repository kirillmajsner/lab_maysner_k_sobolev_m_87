import axios from '../../axios-forum';


export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';


export const fetchCommentsSuccess = comments => ({type: FETCH_COMMENTS_SUCCESS, comments});


export const fetchComments = postId => {
    return dispatch => {
        return axios.get('/comment/' + postId).then(
            response => dispatch(fetchCommentsSuccess(response.data))
        )
    }
};


export const createComment = (commentData, id) => {
    return (dispatch, getState) => {
        const user = getState().users.user;
        return axios.post('/comment', commentData, {headers: {'Authorization': user.token}}).then(
            response => {
                dispatch(fetchComments(id))
            }
        );
    };
};

