import {FETCH_POSTS_SUCCESS, FETCH_ONE_POST} from "../actions/postsActions";

const initialState = {
    posts: [],
    onePost: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_POSTS_SUCCESS:
            return {
                ...state,
                posts: action.posts
            };
        case FETCH_ONE_POST:
            return {...state, onePost: action.post};
        default:
            return state;
    }
};

export default reducer;
