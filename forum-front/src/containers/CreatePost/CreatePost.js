import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {createPost} from "../../store/actions/postsActions";
import PostForm from "../../components/PostForm/PostForm";

class CreatePost extends Component {

    createPost = postData => {
        this.props.onPostCreated(postData).then(() => {
            this.props.history.push('/');
        });

    };

    componentDidMount() {
        if (!this.props.user) {
            this.props.history.push('/login')
        }
    }

    render() {
        return (
            <Fragment>
                <h2>New post</h2>
                <PostForm onSubmit={this.createPost}/>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    onPostCreated: postData => dispatch(createPost(postData))
});


export default connect(mapStateToProps, mapDispatchToProps)(CreatePost);

