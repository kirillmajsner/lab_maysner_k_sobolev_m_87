import React, {Component, Fragment} from 'react';
import {fetchPosts} from "../../store/actions/postsActions";
import {connect} from "react-redux";
import {Card, CardBody, CardTitle, Row} from "reactstrap";
import ForumThumbnail from "../../components/ForumThumbnail/ForumThumbnail";
import {Link} from 'react-router-dom';
import Moment from "react-moment";


class Posts extends Component {
    componentDidMount() {
        this.props.onFetchPosts();
    }

    render() {
        return (
            <Fragment>
                {this.props.posts.map(post => (
                    <Row key={post._id}>
                        <Card body outline color="info"
                              style={{
                                  display: 'inline-block',
                                  border: '2px solid',
                                  margin: '30px'
                              }}>
                            <CardTitle>
                                <Moment format="D MMM YYYY HH:mm" withTitle>{post.datetime}</Moment>
                                <span> by {post.user.username}</span>
                            </CardTitle>
                            <CardBody style={{display: 'inline-block'}}>
                                <ForumThumbnail image={post.image}/>
                            </CardBody>
                            <Link to={`/post/${post._id}`}>
                                {post.title}
                            </Link>
                        </Card>
                    </Row>
                ))}

            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    posts: state.posts.posts,
});

const mapDispatchToProps = dispatch => ({
    onFetchPosts: () => dispatch(fetchPosts())
});

export default connect(mapStateToProps, mapDispatchToProps)(Posts);
