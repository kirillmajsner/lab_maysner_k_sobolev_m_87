import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {createComment, fetchComments} from "../../store/actions/commentActions";
import {Button, Card, CardBody, CardTitle, Form, Input, Row} from "reactstrap";
import ForumThumbnail from "../../components/ForumThumbnail/ForumThumbnail";
import {fetchOnePost} from "../../store/actions/postsActions";


class SinglePost extends Component {

    state = {
        description: '',
        post: this.props.match.params.id
    };
    inputChangeHendler = event => {
        this.setState({[event.target.name]: event.target.value})
    };
    submitChangeHendler = event => {
        event.preventDefault();
        this.props.createComment({...this.state}, this.props.match.params.id)
    };


    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.fetchOnePost(id);
        this.props.fetchComments(id);
    }


    render() {
        return (
            <Fragment>
                {this.props.onePost ? <Row style={{margin: '20px'}} sm={12}>
                    <CardTitle><ForumThumbnail image={this.props.onePost.image}/></CardTitle>
                    <CardBody>
                        <h2>{this.props.onePost.title}</h2>
                        <p>{this.props.onePost.description}</p>
                    </CardBody>
                </Row> : null}


                {!this.props.comments || this.props.comments.length <= 0 ? null :
                    <div>
                        <h2>Comments:</h2>
                        {this.props.comments.map((item) => {
                            return (
                                <Card key={item._id} body inverse
                                      style={{backgroundColor: '#333', borderColor: '#333', margin: '20px'}}>
                                    <CardTitle>Author: {item.user.username}</CardTitle>
                                    <CardBody>{item.description}</CardBody>

                                </Card>
                            )
                        })}</div>}

                {!this.props.user ? null : <Form>
                    <h3>Do you want write comment?</h3>
                    <Input type='text' name='description' value={this.state.description}
                           onChange={this.inputChangeHendler}/>
                    <Button style={{margin: '20px'}} onClick={this.submitChangeHendler}>send</Button>
                </Form>}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    comments: state.comments.comments,
    onePost: state.posts.onePost,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    fetchComments: id => dispatch(fetchComments(id)),
    fetchOnePost: id => dispatch(fetchOnePost(id)),
    createComment: (data, id) => dispatch(createComment(data, id))
});

export default connect(mapStateToProps, mapDispatchToProps)(SinglePost);
